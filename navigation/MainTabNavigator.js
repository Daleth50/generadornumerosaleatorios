import React from 'react';
import { Platform } from 'react-native';
import { createDrawerNavigator, createStackNavigator, DrawerItems } from "react-navigation";

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import Method2 from '../screens/LinksScreen';
import Method3 from '../screens/Method3';
import Method4 from '../screens/Method4';
import Method5 from '../screens/Method5';
import Method6 from '../screens/Method6';
import Method7 from '../screens/Method7';


import SettingsScreen from '../screens/SettingsScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
  title: 'Metodo de los cuadrados medios',
});

HomeStack.navigationOptions = {
  tabBarLabel: 'cuadrados medios',
  title: 'Metodo de los cuadrados medios',
  // tabBarIcon: ({ focused }) => (
  //   <TabBarIcon
  //     focused={focused}
  //     name={
  //       Platform.OS === 'ios'
  //         ? `ios-information-circle${focused ? '' : '-outline'}`
  //         : 'md-information-circle'
  //     }
  //   />
  // ),
};

const Method2Stack = createStackNavigator({
  Method2: Method2,
  title: 'Algoritmo de productos medios',
});

Method2Stack.navigationOptions = {
  title: 'Algoritmo de productos medios',
};

const Method3Stack = createStackNavigator({
  Method3: Method3,
  title: 'Algoritmo de multiplicador constante',
});

Method3Stack.navigationOptions = {
  title: 'Algoritmo de multiplicador constante',
};

const Method4Stack = createStackNavigator({
  Method4: Method4,
  title: 'Algoritmo lineal',
});

Method4Stack.navigationOptions = {
  title: 'Algoritmo lineal',
};

const Method5Stack = createStackNavigator({
  Method5: Method5,
  title: 'Algoritmo congruencial multiplicativo',
});

Method5Stack.navigationOptions = {
  title: 'Algoritmo congruencial multiplicativo',
};

const Method6Stack = createStackNavigator({
  Method6: Method6,
  title: 'Algoritmo congruencial aditivo',
});

Method6Stack.navigationOptions = {
  title: 'Algoritmo congruencial aditivo',
};

const Method7Stack = createStackNavigator({
  Method7: Method7,
  title: 'Algoritmo congruencial cuadratico',
});

Method7Stack.navigationOptions = {
  title: 'Algoritmo congruencial cuadratico',
};

const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
});



SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
    />
  ),
};
export default createDrawerNavigator({
  HomeStack,
  Method2Stack,
  Method3Stack,
  Method4Stack,
  Method5Stack,
  Method6Stack,
  Method7Stack



})
// export default createBottomTabNavigator({
//   // HomeStack,
//   // LinksStack,
//   // SettingsStack,
// });

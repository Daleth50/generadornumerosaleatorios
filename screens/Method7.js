import React from 'react';
import {
  FlatList,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  Content, Container, Header, Input, List, Item, Label,
  Col, Form, Button, FooterTab, Footer, Spinner, Body, Grid, Row, Left, Right, ListItem
} from 'native-base';
export default class Method7 extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      seed1: 0,
      a: '',
      b: '',
      c: '',
      g: '',
      n: '',
      iterations: 1,
      ban: false,
      working: false,
      finished: false,
      resultsArray: []

    }
  }

  checkA(value){
    v= parseInt(value);
    if(v % 2 == 0 && v > 2){
      this.setState({
        a: value,
        finished: false,
        resultsArray: [],
        ban: false
      })
      return
    }
    this.setState({
      a: value,
      finished: false,
      resultsArray: [],
      ban: true
    })
  }

  checkC(value){
    v= parseInt(value);
    if(v % 2 != 0 ){
      this.setState({
        c: value,
        finished: false,
        resultsArray: [],
        ban: false
      })
      return
    }
    this.setState({
      c: value,
      finished: false,
      resultsArray: [],
      ban: true
    })
  }

  checkB(value){
    v= parseInt(value);
    const {a} = this.state;
    if( parseInt(a) > 2){
      if((parseInt(value)-parseInt(a)) % 4 == 1){
        this.setState({
          b: value,
          finished: false,
          resultsArray: [],
          ban: false
        })
        return
      }
      this.setState({
        b: value,
        finished: false,
        resultsArray: [],
        ban: true
      })
      
    }
    this.setState({
      b: value,
      finished: false,
      resultsArray: [],
      ban: true
    })
  }


  calculeResults() {
    const {seed1,a,b,c,g} = this.state;
    let {resultsArray, iterations} = this.state;
    let Xo = seed1;
    while(iterations > 0){
     
      let m = Math.pow(2,g);
      let ax2 = a*(Xo*Xo);
      let ax2bx = ax2+(b*Xo);
      let ax2bxc = ax2bx + parseInt(c);
      let ax2bxcmod = ax2bxc % m;
      let Na = ax2bxcmod / (m-1);
      let res = {
        Xo,
        ax2,
        ax2bx,
        ax2bxc,
        ax2bxcmod,
        Na      
      }
      resultsArray.push(res);
      Xo = ax2bxcmod;
      iterations--;
    }
   
    this.setState({
      resultsArray,
      finished: true
    })
  }
  render() {
    const { seed1, ban, iterations, working, finished, resultsArray, a, b, c, g } = this.state;
    if (working) {
      return (<Spinner></Spinner>);
    }
    return (
      <Content>
        <Container>
          <Header/>
       
          <Grid>
            <Col>
             {!finished ?  
              <Form>
                {
                  ban ?
                    <Form>
                      <Item error fixedLabel>
                        <Label>
                          Ingresa la semilla:
                      </Label>
                        <Input keyboardType='number-pad' value={seed1 + ''} maxLength={4} onChangeText={(text) => this.setState({
                          seed1:text
                        }) } />
                      </Item>
                      <Item error fixedLabel>
                        <Label>
                          Ingresa a:
                       </Label>
                        <Input keyboardType='number-pad' value={a + ''} maxLength={4} onChangeText={(text) => this.checkA(text)} />
                      </Item>
                      <Item error fixedLabel>
                        <Label>
                          Ingresa la constante b:
                       </Label>
                        <Input keyboardType='number-pad' value={b + ''} maxLength={4} onChangeText={(text) => this.checkB(text)} />
                      </Item>
                      <Item error fixedLabel>
                        <Label>
                          Ingresa la c:
                       </Label>
                        <Input keyboardType='number-pad' value={c + ''} maxLength={4} onChangeText={(text) => this.checkC(text)} />
                      </Item>
                      <Item error fixedLabel>
                        <Label>
                          Ingresa la g:
                       </Label>
                        <Input keyboardType='number-pad' value={g + ''} maxLength={4} onChangeText={(text) => this.setState({
                          g : text
                        }) } />
                      </Item>
                    </Form>
                    :
                    <Form>
                       <Item regular fixedLabel>
                        <Label>
                          Ingresa la semilla:
                      </Label>
                        <Input error keyboardType='number-pad' value={seed1 + ''} maxLength={4} onChangeText={(text) => this.setState({
                          seed1:text
                        }) } />
                      </Item>
                      <Item regular fixedLabel>
                        <Label>
                          Ingresa la a:
                       </Label>
                        <Input error keyboardType='number-pad' value={a + ''} maxLength={4} onChangeText={(text) => this.checkA(text)} />
                      </Item>
                      <Item regular fixedLabel>
                        <Label>
                          Ingresa la b:
                       </Label>
                        <Input error keyboardType='number-pad' value={b + ''} maxLength={4} onChangeText={(text) => this.checkB(text)} />
                      </Item>
                      <Item regular fixedLabel>
                        <Label>
                          Ingresa la c:
                       </Label>
                        <Input error keyboardType='number-pad' value={c + ''} maxLength={4} onChangeText={(text) => this.checkC(text)} />
                      </Item>
                      <Item regular fixedLabel>
                        <Label>
                          Ingresa la g:
                       </Label>
                        <Input error keyboardType='number-pad' value={g + ''} maxLength={4} onChangeText={(text) => this.setState({
                          g:text
                        }) } />
                      </Item>
                      
                    </Form>
                }
                <Item fixedLabel>
                  <Label>Iteraciones: </Label>
                  <Input keyboardType='number-pad' value={iterations + ''} onChangeText={(text) => this.setState({ iterations: text, finished: false, resultsArray: [] })} />
                </Item>
           
              </Form>
               :null}
              {finished ?
                <List>
                  <FlatList style={{ flex: 0 }}
                    data={resultsArray}
                    initialNumToRender={resultsArray.length}
                    renderItem={({ item, index }) => <Item fixedLabel>
                      <ListItem>
                        <Text>Iteracion n.{index += 1}{"\n"}
                          Xo: {item.Xo}{"\n"}
                          aX2: {item.ax2} {"\n"}
                          aX2+bx: {item.ax2bx} {"\n"}
                          aX2+bx+c: {item.ax2bxc} {"\n"}
                          aX2+bx+cmod: {item.ax2bxcmod} {"\n"}
                          Na: {item.Na}</Text>
                      </ListItem>
                    </Item>}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </List>
                : null}
            </Col>
          </Grid>
        
           {!finished ?  
             <Footer transparent>
            <FooterTab transparent>                             
                {!ban ?
                <Button info onPress={() => this.calculeResults()}>
                  <Text>Crear numeros aleatorios</Text>
                </Button> :
                <Button disabled disabled={true} onPress={() => this.calculeResults()}>
                  <Text>Crear numeros aleatorios</Text>
                </Button>
                }           
              </FooterTab>
          </Footer>  : 
          <Footer transparent>
          <FooterTab transparent>  
          <Button info onPress={() => this.setState({finished: false, resultsArray: [] })}>
          <Text>Volver a insertar datos</Text>
         </Button>
         </FooterTab>
          </Footer> }
        </Container>
      </Content>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});

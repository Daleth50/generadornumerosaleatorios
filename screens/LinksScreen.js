import React from 'react';
import {
  FlatList,
  Platform,
  Alert,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  Content, Container, Header, Input, List, Item, Label,
  Col, Form, Button, FooterTab, Footer, Spinner, Body, Grid, Row, Left, Right, ListItem
} from 'native-base';
export default class Method2 extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      seed1: '',
      seed2: '',
      iterations: 1,
      validMedia: false,
      validVarianza: false,
      ban: false,
      working: false,
      finished: false,
      resultsArray: [],
      total: 0,
      media: 0.0

    }

  }
  checkSeed1(text) {
    {
      if (text.length != 4) {
        this.setState({
          seed1: text,
          finished: false,
          resultsArray: [],
          ban: true
        })
        return;
      }
      this.setState({
        seed1: text,
        finished: false,
        resultsArray: [],
        ban: false
      })
    }
  }
  checkSeed2(text) {
    {
      if (text.length != 4) {
        this.setState({
          seed2: text,
          finished: false,
          resultsArray: [],
          ban: true
        })
        return;
      }
      this.setState({
        seed2: text,
        finished: false,
        resultsArray: [],
        ban: false
      })
    }
  }
  calculeResults() {
    const { seed1, seed2, iterations } = this.state;
    let { resultsArray, } = this.state;
    let con = parseInt(iterations);
    let actuallSeed1 = parseInt(seed1);
    let actuallSeed2 = parseInt(seed2);
    while (con > 0) {
      let plus = actuallSeed1 * actuallSeed2;
      if (plus + ''.length != 8) {
        plus = plus * 10;
      }
      plus = plus + '';
      let number = plus.substring(2, 6);
      let resu = parseInt(number) / 10000;
      isNaN(resu) ? con = 0 : resu;
      let res = { seed1: actuallSeed1, seed2: actuallSeed2, result: resu };
      resultsArray.push(res);
      actuallSeed1 = parseInt(actuallSeed2);
      actuallSeed2 = isNaN(parseInt(number)) ? con = 0 : parseInt(number);
      con--;
    }
    this.setState({
      finished: true
    })
  }

  calculevalidMedia(){
    let {resultsArray, iterations} = this.state;
    let total = 0;
    resultsArray.map((element, index) => {
      total+=(element.result);
    });
    let media = total/iterations;
    let z0025 = 1.95996398454005;
    let data1 = Math.pow((12*iterations),0.5);
    let data2 = 1/data1;
    let Li = 0.5- data2;
    let Ls = 0.5+ data2;
    (media<= Ls && media>= Li) ?(
       this.setState({validMedia: true, media}),
      Alert.alert(
        'Esta dentro de los limites',
        'Media: '+media+'\nLi: '+Li+ '\nLs: '+Ls,
        [{text: 'OK'}],
        { cancelable: false }
      )
    )        
    :
    (
      this.setState({validMedia: false, media}),
      Alert.alert(
      'Esta fuera de los limites',
      'Media: '+media+'\nLi: '+Li+ '\nLs: '+Ls,
      [{text: 'OK'}],
      { cancelable: false }
      )
    ) 
    
  }
  calculevalidVarianza(){
    let {resultsArray, iterations, media} = this.state;   
    media = media.toFixed(6);
    media = parseFloat(media); 
    let naM = [];
    let naM2= [];
    let varianza = 0;
    resultsArray.forEach(element => {
      let res = element.result - media;
      naM.push(res);
    });
    naM.forEach(element => {
      let res = Math.pow(element,2);
      naM2.push(res);
    });
    naM2.forEach(element => {
      varianza+= element;
    });
    varianza = varianza/(iterations-1);
    let x21= 31.5549164626671;
    let x22= 70.2224135664345;
    let Li= (x21/(12*(iterations-1)));
    let Ls= (x22/(12*(iterations-1)));

    (varianza<= Ls && varianza>= Li) ?(
      this.setState({validVarianza: true, varianza}),
     Alert.alert(
       'Esta dentro de los limites',
       'varianza: '+varianza+'\nLi: '+Li+ '\nLs: '+Ls,
       [{text: 'OK'}],
       { cancelable: false }
     )
   )        
   :
   (
     this.setState({validVarianza: false, varianza}),
     Alert.alert(
     'Esta fuera de los limites',
     'varianza: '+varianza+'\nLi: '+Li+ '\nLs: '+Ls,
     [{text: 'OK'}],
     { cancelable: false }
     )
   ) 



  }

  render() {
    const { seed1, seed2, ban, iterations, working, finished, resultsArray, validMedia, validVarianza } = this.state;
    if (working) {
      return (<Spinner/>);
    }
    return (
        <Container>
          <Content>
          <Header />
          <Grid>
            <Col>
              <Form>
                {
                  ban ?
                    <Form>
                      <Item error fixedLabel>
                        <Label>
                          Ingresa la semilla 1:
                      </Label>
                        <Input keyboardType='number-pad' value={seed1 + ''} maxLength={4} onChangeText={(text) => this.checkSeed1(text)} />
                      </Item>
                      <Item error fixedLabel>
                        <Label>
                          Ingresa la semilla 2:
                    </Label>
                        <Input keyboardType='number-pad' value={seed2 + ''} maxLength={4} onChangeText={(text) => this.checkSeed2(text)} />
                      </Item>
                    </Form>
                    :
                    <Form>
                      <Item regular fixedLabel>
                        <Label>
                          Ingresa la semilla 1:
                    </Label>
                        <Input error keyboardType='number-pad' value={seed1 + ''} maxLength={4} onChangeText={(text) => this.checkSeed1(text)} />
                      </Item>
                      <Item regular fixedLabel>
                        <Label>
                          Ingresa la semilla 2:
                  </Label>
                        <Input error keyboardType='number-pad' value={seed2 + ''} maxLength={4} onChangeText={(text) => this.checkSeed2(text)} />
                      </Item>
                    </Form>

                }
                <Item fixedLabel>
                  <Label>Iteraciones: </Label>
                  <Input keyboardType='number-pad' value={iterations + ''} onChangeText={(text) => this.setState({ iterations: text, finished: false, resultsArray: [] })} />
                </Item>

              </Form>

              {finished ?

                <List>
                  <FlatList style={{ flex: 0 }}
                    data={resultsArray}
                    // renderItem={({ item, index }) => this.renderItem(item, index)}
                    renderItem={({ item, index }) => <Item fixedLabel>
                      <ListItem>
                        <Text>Iteracion n.{index += 1}{"\n"}
                          Semilla 1: {item.seed1}{"\n"}
                          Semilla 2: {item.seed2} {"\n"}
                          Numero aleatorio: {item.result}</Text>
                      </ListItem>
                    </Item>}
                    keyExtractor={(item, index) => index.toString()}
                    initialNumToRender={resultsArray.length}
                    extraData = {this.state}
                  />
                </List>
                : null}
            </Col>
          </Grid>
          {
            !finished ?
            <Footer transparent>
            <FooterTab transparent>
              {!ban ?
                <Button info onPress={() => this.calculeResults()}>
                  <Text>Crear numeros aleatorios</Text>
                </Button> :
                <Button disabled disabled={true} onPress={() => this.calculeResults()}>
                  <Text>Crear numeros aleatorios</Text>
                </Button>
              }

            </FooterTab>
          </Footer>
            :
            <Footer transparent>
            <FooterTab transparent>              
                <Button info onPress={() => this.calculevalidMedia()}>
                  <Text>Validar Media</Text>
                </Button> 
                {
                  validMedia ?
                  <Button info onPress={() => this.calculevalidVarianza()}>
                  <Text>Validar Varianza</Text>
                </Button>
                  :
                <Button disabled disabled={true} onPress={() => this.calculevalidVarianza()}>
                  <Text>Validar Varianza</Text>
                </Button>
                }
                
            </FooterTab>
          </Footer>
          }  
           </Content>        
        </Container>
     
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});

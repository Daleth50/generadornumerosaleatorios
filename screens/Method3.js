import React from 'react';
import {
  FlatList,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  Content, Container, Header, Input, List, Item, Label,
  Col, Form, Button, FooterTab, Footer, Spinner, Body, Grid, Row, Left, Right, ListItem
} from 'native-base';

export default class Method3 extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      seed: '',
      iterations: 1,
      ban: false,
      working: false,
      finished: false,
      resultsArray: []

    }

  }
  checkSeed(text) {
    {
      if (text.length != 4) {
        this.setState({
          seed: text,
          finished: false,
          resultsArray: [],
          ban: true
        })
        return;
      }
      this.setState({
        seed: text,
        finished: false,
        resultsArray: [],
        ban: false
      })
    }
  }
  calculeResults() {
    const { seed, iterations } = this.state;
    let { resultsArray, } = this.state;
    let con = parseInt(iterations);
    let actuallSeed = parseInt(seed);
    while (con > 0) {
      let pow = Math.pow(actuallSeed, 2);
      if (pow + ''.length != 8) {
        pow = pow * 10;
      }
      pow = pow + '';
      let number = pow.substring(2, 6);
      let resu = parseInt(number) / 10000;
      isNaN(resu) ? con = 0 : resu;
      let res = { seed: actuallSeed, result: resu };
      resultsArray.push(res);
      // actuallSeed =  parseInt(number);
      actuallSeed = isNaN(parseInt(number)) ? con = 0 : parseInt(number);
      con--;
    }
    this.setState({
      finished: true
    })
  }
  renderItem(item, index) {
    console.log(index)
    return (
      <Item>
        <ListItem>
          <Body>
            <Text>Iteracion n.{index += 1}</Text>
            <Text> Semilla: {item.seed}</Text>
          </Body>
          <Right>
            <Text> Numero aleatorio: {item.result}</Text>
          </Right>
        </ListItem >
      </Item>
    )
  }

  render() {
    const { seed, ban, iterations, working, finished, resultsArray } = this.state;
    if (working) {
      return (<Spinner></Spinner>);
    }
    return (
      <Content>
        <Container>
          <Header/>
          <Grid>
            <Col>
              <Form>
                {
                  ban ?
                    <Item error fixedLabel>
                      <Label>
                        Ingresa la semilla:
                      </Label>
                      <Input keyboardType='number-pad' value={seed + ''} maxLength={4} onChangeText={(text) => this.checkSeed(text)} />
                    </Item>
                    :
                    <Item regular fixedLabel>
                      <Label>
                        Ingresa la semilla:
                      </Label>
                      <Input error keyboardType='number-pad' value={seed + ''} maxLength={4} onChangeText={(text) => this.checkSeed(text)} />
                    </Item>
                }
                <Item fixedLabel>
                  <Label>Iteraciones: </Label>
                  <Input keyboardType='number-pad' value={iterations + ''} onChangeText={(text) => this.setState({ iterations: text, finished: false, resultsArray: [] })} />
                </Item>

              </Form>

              {finished ?

                <List>
                  <FlatList
                    data={resultsArray}
                    // renderItem={({ item, index }) => this.renderItem(item, index)}
                    renderItem={({ item, index }) => <Item fixedLabel>
                      <ListItem>
                        {/* <Text>Iteracion n.{index += 1}</Text> */}
                        <Text> Semilla: {item.seed}</Text>
                        <Text> Numero aleatorio: {item.result}</Text>
                      </ListItem>
                    </Item>}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </List>
                : null}
            </Col>
          </Grid>
          <Footer>
            <FooterTab>
              {!ban ?
                <Button info onPress={() => this.calculeResults()}>
                  <Text>Crear numeros aleatorios</Text>
                </Button> :
                <Button disabled disabled={true} onPress={() => this.calculeResults()}>
                  <Text>Crear numeros aleatorios</Text>
                </Button>
              }

            </FooterTab>
          </Footer>
        </Container>
      </Content>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});

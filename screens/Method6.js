import React from 'react';
import {
  FlatList,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  Content, Container, Header, Input, List, Item, Label,
  Col, Form, Button, FooterTab, Footer, Spinner, Body, Grid, Row, Left, Right, ListItem
} from 'native-base';
export default class Method6 extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      seeds: [],
      nSeeds: 0,
      iterations: 1,
      g: 0,
      ban: false,
      working: false,
      finished : false,
      ready: false,
      resultsArray: []

    }

  }
  checkNSeed(text) {
    {
      console.log(parseInt(text) )
      if (parseInt(text) >= 2) {
        this.setState({
          nSeeds: text,
          resultsArray: [],
          ban: false
        })
        return;
      }
      this.setState({
        nSeeds: text,
        resultsArray: [],
        ban: true
      })
    }
  }

  pushSeed(value, index){
    let {seeds, nSeeds} = this.state;
    this.ban = false;
    seeds.forEach(element => {
      if(element[0] == index){
        element[1] = isNaN(parseInt(value)) ? 0 :parseInt(value);
        this.ban = true;
      }
    });

    if(!this.ban){
      seeds.push([index,isNaN(parseInt(value)) ? 0 :parseInt(value)])
    }
    if(seeds.length == nSeeds){
      this.setState({
        seeds,
        ready : true
      })
      return;
    }
    this.setState({
      seeds,
      ready : false
    })
  }
  renderAskSeeds(){
    const {nSeeds} = this.state;
    let arr = [];
    // console.log(nSeeds)
    for (let index = 0; index < nSeeds; index++) {
         arr.push(
           <Form key={index}>
              <Item fixedLabel>
                <Label>
                Ingresa un numero: {index+1}
                </Label>
              <Input keyboardType='number-pad' maxLength={4} onChangeText={(text) => this.pushSeed(text,index)} />
              </Item>
              </Form>
         )    
    }
    return arr;    
  }

  calculeResults(){
    let {seeds,g,resultsArray, iterations} = this.state;
    seeds.forEach(element =>{
      resultsArray.push({
        xo: element[1],
        xixn: "",
        m: "",
        xixnmod: "",
        Na: ""
      })
    })
    let con = parseInt(iterations-1);
    let ind = 0;
    let m = Math.pow(g,7);
    let xixn = resultsArray[ind].xo + resultsArray[resultsArray.length-1].xo;
      let xixnmod = xixn % m;
      let Na =xixnmod/(m-1);
      let res = {
        xo: resultsArray[resultsArray.length-1].xo,
        xixn: xixn,
        m: m,
        xixnmod: xixnmod,
        Na: Na
      }
      resultsArray.pop()
      resultsArray.push(res)
      // resultsArray[resultsArray.length-1] = res;
      let xo =xixnmod;
      ind ++
    while (con > 0) {
      resultsArray.push({
        xo: xo,
        xixn: "",
        m: "",
        xixnmod: "",
        Na: ""
      })
      let xixn = resultsArray[ind].xo + resultsArray[resultsArray.length-1].xo;
      let xixnmod = xixn % m;
      let Na =xixnmod/(m-1);
      let res = {
        xo: xo,
        xixn: xixn,
        m: m,
        xixnmod: xixnmod,
        Na: Na
      }
      resultsArray.pop()
      resultsArray.push(res)
      xo =xixnmod;
      ind ++
      con --
    }
    this.setState({
      resultsArray,
      finished: true
    })


  }

  render() {
    const { ban, iterations, working, ready, resultsArray, nSeeds,finished,g } = this.state;
    if (working) {
      return (<Spinner></Spinner>);
    }
   
    return (
      <Content>
        <Container>
          <Header />
          <Grid>
            <Col>
              <Form>
                {
                  ban ?
                    <Form>
                      <Item error fixedLabel>
                        <Label>
                          Ingresa el numero de semillas:
                      </Label>
                        <Input keyboardType='number-pad' value={nSeeds + ''} maxLength={4} onChangeText={(text) => this.checkNSeed(text)} />
                      </Item>
                    </Form>
                    :
                    <Form>
                      <Item fixedLabel>
                        <Label>
                          Ingresa el numero de semillas:
                      </Label>
                        <Input keyboardType='number-pad' value={nSeeds + ''} maxLength={4} onChangeText={(text) => this.checkNSeed(text)} />
                      </Item>
                    </Form>
                }
                 <Item fixedLabel>
                  <Label>valor de g: </Label>
                  <Input keyboardType='number-pad' value={g + ''} onChangeText={(text) => this.setState({ g: text, ready: false, resultsArray: [], finished: false })} />
                </Item>
                <Item fixedLabel>
                  <Label>Iteraciones: </Label>
                  <Input keyboardType='number-pad' value={iterations + ''} onChangeText={(text) => this.setState({ iterations: text, ready: false, resultsArray: [], finished: false })} />
                </Item>
               

              </Form>

              {finished ?
                <List>
                  <FlatList
                    data={resultsArray}
                    // renderItem={({ item, index }) => this.renderItem(item, index)}
                    renderItem={({ item, index }) => <Item fixedLabel>
                      <ListItem>
                        <Text>Iteracion n.{index += 1}{"\n"}
                          Xo: {item.xo}{"\n"}
                          XiXn: {item.xixn} {"\n"}
                          m: {item.m} {"\n"}
                          xixnMod: {item.xixnmod} {"\n"}
                          Numero aleatorio: {item.Na}</Text>
                      </ListItem>
                    </Item>}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </List>
                : 
               this.renderAskSeeds()
                }
            </Col>
          </Grid>
          {!finished ? 
           <Footer>
           <FooterTab>
             {ready ?
               <Button info onPress={() => this.calculeResults()}>
                 <Text>Crear numeros aleatorios</Text>
               </Button> :
               <Button disabled disabled={true} onPress={() => this.calculeResults()}>
                 <Text>Crear numeros aleatorios</Text>
               </Button>
             }

           </FooterTab>
         </Footer>
         : null
        
        }
         
        </Container>
      </Content>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});

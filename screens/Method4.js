import React from 'react';
import {
  FlatList,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  Content, Container, Header, Input, List, Item, Label,
  Col, Form, Button, FooterTab, Footer, Spinner, Body, Grid, Row, Left, Right, ListItem
} from 'native-base';
var isCoprime = require('is-coprime');
export default class Method4 extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      seed1: '',
      k: '',
      g: '',
      c: '',
      m: '',
      iterations: 1,
      ban: false,
      working: false,
      finished: false,
      resultsArray: []

    }

  }
  checkSeed1(text) {
    {
      if (text.length != 2) {
        this.setState({
          seed1: text,
          finished: false,
          resultsArray: [],
          ban: true
        })
        return;
      }
      this.setState({
        seed1: text,
        finished: false,
        resultsArray: [],
        ban: false
      })
    }
  }
  checkK(text) {
    {
      let a = 1 + 4 * parseInt(text);
      this.setState({
        k: text,
        a: a,
        finished: false,
        resultsArray: [],
        ban: false
      })
    }
  }
  checkG(text) {
    {
      let m = Math.pow(2, parseInt(text))
      this.setState({
        g: text,
        m: m,
        finished: false,
        resultsArray: [],
        ban: false
      })
    }
  }
  checkC(text) {//validar que sea primo relativo
    {
      let { g, m } = this.state;
      if (m) {
        try {
          let coprime = isCoprime(parseInt(text), parseInt(m));
          if (!coprime) {
            this.setState({
              c: parseInt(text),
              finished: false,
              resultsArray: [],
              ban: true
            })
            return;
          }
          this.setState({
            c: parseInt(text),
            finished: false,
            resultsArray: [],
            ban: false
          })
        } catch (error) {
          console.log(error)
          text = 0;
        }
      }
      this.setState({
        c: parseInt(text),
        finished: false,
        resultsArray: [],
        ban: false
      })
    }
  }

  calculeResults() {
    const { seed1, a, c, m, iterations } = this.state;
    let { resultsArray } = this.state;
    let con = parseInt(iterations);
    let xi = seed1;
    while (con > 0) {
      let axi = a * xi;
      let axic = axi + c;
      let axicmod = axic % m;
      let R = axicmod / (m - 1);
      let res = {
        seed1: xi,
        yo: axic,
        xi: axicmod,
        ri: R
      }
      resultsArray.push(res);
      con--;
      xi = axicmod;
    }
    this.setState({
      resultsArray,
      finished: true
    })
  }
  render() {
    const { seed1, ban, iterations, working, finished, resultsArray, k, g, c, } = this.state;
    if (working) {
      return (<Spinner></Spinner>);
    }
    return (
      <Content>
        <Container>
          <Header />
          <Grid>
            <Col>
              <Form>
                {
                  ban ?
                    <Form>
                      <Item error fixedLabel>
                        <Label>
                          Ingresa la semilla:
                      </Label>
                        <Input keyboardType='number-pad' value={seed1 + ''} maxLength={4} onChangeText={(text) => this.checkSeed1(text)} />
                      </Item>
                      <Item error fixedLabel>
                        <Label>
                          Ingresa la constante k:
                       </Label>
                        <Input keyboardType='number-pad' value={k + ''} maxLength={4} onChangeText={(text) => this.checkK(text)} />
                      </Item>
                      <Item error fixedLabel>
                        <Label>
                          Ingresa la constante g:
                       </Label>
                        <Input keyboardType='number-pad' value={g + ''} maxLength={4} onChangeText={(text) => this.checkG(text)} />
                      </Item>
                      <Item error fixedLabel>
                        <Label>
                          Ingresa la c:
                       </Label>
                        <Input keyboardType='number-pad' value={c + ''} maxLength={4} onChangeText={(text) => this.checkC(text)} />
                      </Item>
                    </Form>
                    :
                    <Form>
                      <Item regular fixedLabel>
                        <Label>
                          Ingresa la semilla:
                    </Label>
                        <Input error keyboardType='number-pad' value={seed1 + ''} maxLength={4} onChangeText={(text) => this.checkSeed1(text)} />
                      </Item>
                      <Item regular fixedLabel>
                        <Label>
                          Ingresa la constante k:
                       </Label>
                        <Input error keyboardType='number-pad' value={k + ''} maxLength={4} onChangeText={(text) => this.checkK(text)} />
                      </Item>
                      <Item regular fixedLabel>
                        <Label>
                          Ingresa la constante g:
                       </Label>
                        <Input error keyboardType='number-pad' value={g + ''} maxLength={4} onChangeText={(text) => this.checkG(text)} />
                      </Item>
                      <Item regular fixedLabel>
                        <Label>
                          Ingresa la c:
                       </Label>
                        <Input error keyboardType='number-pad' value={c + ''} maxLength={4} onChangeText={(text) => this.checkC(text)} />
                      </Item>
                    </Form>
                }
                <Item fixedLabel>
                  <Label>Iteraciones: </Label>
                  <Input keyboardType='number-pad' value={iterations + ''} onChangeText={(text) => this.setState({ iterations: text, finished: false, resultsArray: [] })} />
                </Item>

              </Form>

              {finished ?

                <List>
                  <FlatList style={{ flex: 0 }}
                    data={resultsArray}
                    initialNumToRender={resultsArray.length}
                    // renderItem={({ item, index }) => this.renderItem(item, index)}
                    renderItem={({ item, index }) => <Item fixedLabel>
                      <ListItem>
                        {/* <Text>Iteracion n.{index += 1}{"\n"} */}
                        <Text>Semilla: {item.seed1}{"\n"}
                          Yo: {item.yo} {"\n"}
                          Xi: {item.xi} {"\n"}
                          Numero aleatorio: {item.ri}</Text>
                      </ListItem>
                    </Item>}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </List>
                : null}
            </Col>
          </Grid>
          <Footer>
            <FooterTab>
              {!ban ?
                <Button info onPress={() => this.calculeResults()}>
                  <Text>Crear numeros aleatorios</Text>
                </Button> :
                <Button disabled disabled={true} onPress={() => this.calculeResults()}>
                  <Text>Crear numeros aleatorios</Text>
                </Button>
              }

            </FooterTab>
          </Footer>
        </Container>
      </Content>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
